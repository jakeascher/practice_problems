function assertEquals(actual, expected) {
  // This if statement is necessary in order to properly compare
  // the contents of two arrays for equality. Because arrays (and objects)
  // are passed by reference in JavaScript, we need to stringify them
  // so that we have two distinct objects in memory to compare.
  if (Array.isArray(actual) && Array.isArray(expected)) {
    actual = JSON.stringify(actual);
    expected = JSON.stringify(expected);
  }

  if (actual !== expected) {
    console.log(`❌ Expected ${expected} but got ${actual}`);
  } else {
    console.log(`✅ Got expected value: ${actual}`);
  }
}

const uppercaseAll = function (strings) {
  const result = [];
  for (let string of strings) {
    result.push(string.toUpperCase());
  }
  return result;
};

const uppercaseAll2 = function (strings) {
  return strings.map(string => string.toUpperCase());
};

const runTests = function () {
  const stringsToTest = ["semicolons in javascript, not in python", "snakes", "tigers", "bears"];
  const expectedOutput = ['SEMICOLONS IN JAVASCRIPT, NOT IN PYTHON', 'SNAKES', 'TIGERS', 'BEARS'];

  assertEquals(uppercaseAll(stringsToTest), expectedOutput);
  assertEquals(uppercaseAll2(stringsToTest), expectedOutput);
};

runTests();
