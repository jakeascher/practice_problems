import unittest

def uppercase_all(strings):
    result = []
    for string in strings:
        result.append(string.upper())
    return result

def uppercase_all2(strings):
    return [string.upper() for string in strings]

class TestUppercaseAll(unittest.TestCase):
    def test_one(self):
        expected = ['SEMICOLONS IN JAVASCRIPT, NOT IN PYTHON', 'SNAKES']
        actual = uppercase_all(["semicolons in javascript, not in python", "snakes"])
        self.assertEqual(expected, actual)

    def test_two(self):
        expected = ['SEMICOLONS IN JAVASCRIPT, NOT IN PYTHON', 'SNAKES']
        actual = uppercase_all2(["semicolons in javascript, not in python", "snakes"])
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
