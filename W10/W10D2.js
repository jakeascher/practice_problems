function assertEquals(actual, expected) {
  // This if statement is necessary in order to properly compare
  // the contents of two arrays for equality. Because arrays (and objects)
  // are passed by reference in JavaScript, we need to stringify them
  // so that we have two distinct objects in memory to compare.
  if (Array.isArray(actual) && Array.isArray(expected)) {
    actual = JSON.stringify(actual);
    expected = JSON.stringify(expected);
  }

  if (actual !== expected) {
    console.log(`❌ Expected ${expected} but got ${actual}`);
  } else {
    console.log(`✅ Got expected value: ${actual}`);
  }
}

const shorterThan5 = function(strings) {
  const results = [];

  for (let string of strings) {
    if (string.length < 5) {
      results.push(string);
    }
  }

  return results;
};

const shorterThan = function(strings, maxLength) {
  const results = [];

  for (let string of strings) {
    if (string.length < maxLength) {
      results.push(string);
    }
  }

  return results;
}

const shorterThanX = function(strings, maxLength) {
  return strings.filter(string => string.length < maxLength);
};

const runTests = function () {
  const stringsToTest = ['this', 'is', 'a', 'super', 'lengthy', 'list', 'of', 'strings'];
  const expectedOutput = ['this', 'is', 'a', 'list', 'of'];

  assertEquals(shorterThan5(stringsToTest), expectedOutput);
  assertEquals(shorterThan(stringsToTest, 5), expectedOutput);
  assertEquals(shorterThanX(stringsToTest, 5), expectedOutput);
};

runTests();
