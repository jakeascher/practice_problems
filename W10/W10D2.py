import unittest

def shorter_than_5(strings):
    result = []

    for string in strings:
        if len(string) < 5:
            result.append(string)

    return result

def shorter_than(strings, max_length):
    result = []

    for string in strings:
        if len(string) < max_length:
            result.append(string)

    return result

def shorter_than_X(strings, max_length):
    return [string for string in strings if len(string) < max_length]


class TestPython1(unittest.TestCase):
    def test_one(self):
        expected = ["in", "not", "in"]
        actual = shorter_than_5(["semicolons", "in", "javascript", "not", "in", "python", "snakes"])
        self.assertEqual(expected, actual)

class TestPython2(unittest.TestCase):
    def test_two(self):
        expected = ["in", "not", "in"]
        actual = shorter_than(["semicolons", "in", "javascript", "not", "in", "python", "snakes"], 5)
        self.assertEqual(expected, actual)

class TestPython3(unittest.TestCase):
    def test_three(self):
        expected = ["in", "not", "in"]
        actual = shorter_than_X(["semicolons", "in", "javascript", "not", "in", "python", "snakes"], 5)
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
