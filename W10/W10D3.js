function assertEquals(actual, expected) {
  if (actual !== expected) {
    console.log(`❌ Expected ${expected} but got ${actual}`);
  } else {
    console.log(`✅ Got expected value: ${actual}`);
  }
}

function mean(numbers) {
  let total = 0;
  for (let num of numbers) {
    total += num;
  }
  return total / numbers.length;
}

function mean2(numbers) {
  const total = numbers.reduce((sum, number) => sum + number);
  return total / numbers.length;
}

const runTests = function () {
  assertEquals(mean([3, 33, 333]), 123);
  assertEquals(mean([11, 13, 15]), 13);

  assertEquals(mean2([3, 33, 333]), 123);
  assertEquals(mean2([11, 13, 15]), 13);
};

runTests();
