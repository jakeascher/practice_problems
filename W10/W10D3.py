import unittest
from functools import reduce

def mean(numbers):
    sum = 0
    for number in numbers:
        sum += number
    return sum / len(numbers)

def add_one(sum, number):
    return sum + number

def mean2(numbers):
    return reduce(add_one, numbers) / len(numbers)

class TestMean(unittest.TestCase):
    def test_one(self):
        expected = 123
        actual = mean([3, 33, 333])
        self.assertEqual(expected, actual)
    def test_two(self):
        expected = 13
        actual = mean([11, 13, 15])
        self.assertEqual(expected, actual)

class TestMean2(unittest.TestCase):
    def test_one(self):
        expected = 123
        actual = mean2([3, 33, 333])
        self.assertEqual(expected, actual)
    def test_two(self):
        expected = 13
        actual = mean2([11, 13, 15])
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
