import unittest

def merge_sorted_lists(list1, list2):
    # create a merged_list variable

    # while either list has elements
        # if both lists still have elements
            # if left-most item in list1 < left-most item in list2
                # append left-most item from list1 to merged_list
            # else
                # append left-most item from list2 to merged_list
        # if list1 has been exhausted
            # add remaining elements from list2 to merged_list
        # else (list2 has been exhausted)
            # add remaining elements from list1 to merged_list

    # return merged_list

    merged_list = []

    while len(list1) > 0 or len(list2) > 0:
        if len(list1) > 0 and len(list2) > 0:
            if list1[0] < list2[0]:
                merged_list.append(list1.pop(0))
            else:
                merged_list.append(list2.pop(0))
        elif len(list2) > 0:
            merged_list.append(list2.pop(0))
        else: # list2 is finished
            merged_list.append(list1.pop(0))

    return merged_list


class TestMergeSortedLists(unittest.TestCase):
    def test_one(self):
        expected = [1, 2, 3, 4, 5]
        actual = merge_sorted_lists([1, 3, 5], [2, 4])
        self.assertEqual(expected, actual)

    def test_two(self):
        expected = [-4, -3, -2, -1, 0, 1, 2, 3, 4]
        actual = merge_sorted_lists([-4, -2, 0, 2, 4], [-3, -1, 1, 3])
        self.assertEqual(expected, actual)

    def test_one_empty_list(self):
        expected = [1, 2, 3]
        actual = merge_sorted_lists([1, 2, 3], [])
        self.assertEqual(expected, actual)

    def test_two_empty_lists(self):
        expected = []
        actual = merge_sorted_lists([], [])
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
