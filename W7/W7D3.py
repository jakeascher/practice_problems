def assert_equal(actual, expected):
    """Assert if two values are equal.""" # This is a docstring (https://peps.python.org/pep-0257/#what-is-a-docstring)
    if actual == expected:
        print('✅ Passed')
    else:
        print(f'❌ Failed: {actual} does not equal {expected}')


# 1) - Sort a list of two, and only two, numbers

# For example:
# If the input is [1, 2], it should return [1, 2].
# If the input is [4, 2], then it should return [2, 4].

def sort1(items):
    # if items at index 0 and index 1 need swapped
    if items[0] > items[1]:
        # swap the items
        temp = items[0]
        items[0] = items[1]
        items[1] = temp
    # return the items
    return items

print('Testing first function...')
assert_equal(sort1([1, 2]), [1, 2])
assert_equal(sort1([2, 1]), [1, 2])
# assert_equal(sort1([3, 1, 2]), [1, 2, 3]) # This will fail!


# 2) - Extend our sorting function to handle lists of len > 2

# For example:
# If the input is [3, 1, 2], it should return [1, 2, 3].

def sort2(items):
    #   for each index except the last
    for i in range(len(items) - 1):    # [0, 1, 2, 3, 4]   -> len = 5
        if items[i] > items[i + 1]:
            (items[i], items[i + 1]) = (items[i + 1], items[i])

    return items

print('Testing second function...')
assert_equal(sort2([1, 2]), [1, 2])
assert_equal(sort2([2, 1]), [1, 2])
assert_equal(sort2([3, 1, 2]), [1, 2, 3])
# assert_equal(sort2([5, 4, 3, 2, 1]), [1, 2, 3, 4, 5]) # This will fail! Why?
# assert_equal(sort2([4, 3, 2, 1, 5]), [1, 2, 3, 4, 5]) # One step closer...
# assert_equal(sort2([3, 2, 1, 4, 5]), [1, 2, 3, 4, 5]) # Almost there...
assert_equal(sort2([2, 1, 3, 4, 5]), [1, 2, 3, 4, 5]) # This will pass. Why?


# 3) - Finish our bubble sort implementation
# Keep sorting until we are finished swapping items

# For example:
# If the input is [5, 4, 3, 2, 1], it should return [1, 2, 3, 4, 5]

def sort3(items):
    while True:
        swapped = False
        for i in range(len(items) - 1):
            if items[i] > items[i+1]:
                (items[i], items[i+1]) = (items[i+1], items[i])
                swapped = True

        if not swapped:
            break
    return items

print('Testing third function...')
assert_equal(sort3([1, 2]), [1, 2])
assert_equal(sort3([2, 1]), [1, 2])
assert_equal(sort3([3, 1, 2]), [1, 2, 3])
assert_equal(sort3([5, 4, 3, 2, 1]), [1, 2, 3, 4, 5])
