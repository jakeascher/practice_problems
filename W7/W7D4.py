import unittest

def bubble_sort(items):
    iterations = 0
    while True:
        made_a_swap = False

        for i in range(len(items) - 1):
            if items[i] > items[i+1]:
                items[i], items[i+1] = items[i+1], items[i]
                made_a_swap = True

        if not made_a_swap:
            break
        iterations += 1

    print(f'Bubble Sort took {iterations} iterations')
    return items

def swap(items, index_1, index_2):
    items[index_1], items[index_2] = items[index_2], items[index_1]
    return items

def bubble_up(items):
    for i in range(len(items) - 1):
        if items[i] > items[i+1]:
            items[i], items[i+1] = items[i+1], items[i]

    return items

def bubble_down(items):
    for i in range(len(items) - 1, 0, -1):
        if items[i] < items[i-1]:
            items[i], items[i-1] = items[i-1], items[i]

    return items

def cocktail_sort(items):
    while True:
        made_a_swap = False

        # bubble up
        for i in range(len(items) - 1):
            if items[i] > items[i+1]:
                items[i], items[i+1] = items[i+1], items[i]
                made_a_swap = True

        if not made_a_swap:
            break

        # bubble down
        for i in range(len(items) - 1, 0, -1):
            if items[i] < items[i-1]:
                items[i], items[i-1] = items[i-1], items[i]
                made_a_swap = True

        if not made_a_swap:
            break

    return items


class TestSwap(unittest.TestCase):
    def test_one(self):
        actual = swap([8,11], 0, 1)
        expected = [11,8]
        self.assertEqual(actual, expected)

    def test_two(self):
        actual = swap([1,2,3,4], 1, 2)
        expected = [1,3,2,4]
        self.assertEqual(actual, expected)


class TestBubbleUp(unittest.TestCase):
    def test_one(self):
        actual = bubble_up([4,3,2,1])
        expected = [3,2,1,4]
        self.assertEqual(actual, expected)


class TestBubbleDown(unittest.TestCase):
    def test_one(self):
        actual = bubble_down([4,3,2,1])
        expected = [1,4,3,2]
        self.assertEqual(actual, expected)


class TestCocktailSort(unittest.TestCase):
    def test_one(self):
        actual = cocktail_sort([4,1,3,2])
        expected = [1,2,3,4]
        self.assertEqual(actual, expected)

    def test_two(self):
        actual = cocktail_sort([4,1,3,2,5,0,8,7])
        expected = [0,1,2,3,4,5,7,8]
        self.assertEqual(actual, expected)

class TestBubbleSort(unittest.TestCase):
    def test_one(self):
        actual = bubble_sort([4,1,3,2,5,0,8,7])
        expected = [0,1,2,3,4,5,7,8]
        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
