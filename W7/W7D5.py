import json, unittest


def decode_and_pluck(input_json, fields):
    dictionary = json.loads(input_json)
    output = {}
    for key, value in dictionary.items():
        if key in fields:
            output[key] = value
    return output


def dict_to_list(the_dict):
    result = []
    for key in the_dict:
        result.append(key)
        result.append(the_dict[key])
    return result


def find_big_space_objects(space_objects):
    big_objects = []
    for thing in space_objects:
        if thing['estimated_diameter'] >= 1:
            new_dict = {
                'estimated_diameter': thing['estimated_diameter'],
                'impact_probability': thing['impact_probability']
            }
            encoded_dict = json.dumps(new_dict)
            big_objects.append(encoded_dict)
    return big_objects


def list_to_dict(lst):
    answer = {}
    for i in range(0, len(lst), 2):
        try:
            answer[lst[i]] = lst[i + 1]
        except IndexError:
            answer[lst[i]] = None
    return answer


class Test_decode_and_pluck(unittest.TestCase):
    def test_one(self):
        input_json = '{"cat": "fluffy", "dog": "fido", "turtle": "barney", "owl": "charlie"}'
        actual = decode_and_pluck(input_json, ["cat", "turtle"])
        expected = {"cat": "fluffy", "turtle": "barney"}
        self.assertEqual(actual, expected)


class Test_dict_to_list(unittest.TestCase):
    def test_one(self):
        the_dict = {"a":1, "b":2}
        actual = dict_to_list(the_dict)
        expected = ["a", 1, "b", 2]
        self.assertEqual(actual, expected)


class Test_find_big_space_objects(unittest.TestCase):
    def test_one(self):
        objects = [
            {"estimated_diameter": 1.5, "impact_probability": 0.0001, 'mass': 300},
            {"estimated_diameter": 0.5, "impact_probability": 0.0101, 'mass': 30},
            {"estimated_diameter": 10.5, "impact_probability": 0.1001, 'mass': 30000},
        ]
        actual = find_big_space_objects(objects)
        expected = [
            '{"estimated_diameter": 1.5, "impact_probability": 0.0001}',
            '{"estimated_diameter": 10.5, "impact_probability": 0.1001}'
        ]
        self.assertEqual(actual, expected)


class Test_list_to_dict(unittest.TestCase):
    def test_one(self):
        the_list = ["a", 1, "b", 2]
        actual = list_to_dict(the_list)
        expected = {"a":1, "b":2}
        self.assertEqual(actual, expected)

    # How should we handle incorrect input? We are choosing to use 'None'
    # as a value when provided an odd number of items. We could have decided
    # to omit the incomplete property altogether (this would result in
    # needing to change the test you see below).
    def test_incorrect_input(self):
        the_list = ['a', 1, 'b', 2, 'c']
        actual = list_to_dict(the_list)
        expected = {"a":1, "b":2, "c": None}
        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
