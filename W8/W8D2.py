import unittest

def fizzbuzz(maximum):
    result = []

    for i in range(1, maximum + 1):
        if i % 3 == 0 and i % 5 == 0:
            result.append("fizzbuzz")
        elif i % 3 == 0:
            result.append("fizz")
        elif i % 5 == 0:
            result.append("buzz")
        else:
            result.append(str(i))

    return result

def sum_evens(numbers):
    result = 0
    for number in numbers:
        if number % 2 == 0:
            result += number
    return result


class Test_fizz_buzz(unittest.TestCase):
    def test_1(self):
        self.assertEqual(fizzbuzz(0), [])

    def test_2(self):
        self.assertEqual(fizzbuzz(1), ["1"])

    def test_3(self):
        self.assertEqual(fizzbuzz(3), ["1", "2", "fizz"])

    def test_4(self):
        self.assertEqual(fizzbuzz(6), ["1", "2", "fizz", "4", "buzz", "fizz"])

    def test_5(self):
        num = 50
        actual = fizzbuzz(num)
        expected = [
            "1","2","fizz","4","buzz","fizz","7","8","fizz","buzz",
            "11","fizz","13","14","fizzbuzz","16","17","fizz","19","buzz",
            "fizz","22","23","fizz","buzz","26","fizz","28","29","fizzbuzz",
            "31","32","fizz","34","buzz","fizz","37","38","fizz","buzz",
            "41","fizz","43","44","fizzbuzz","46","47","fizz","49","buzz"
        ]
        self.assertEqual(actual, expected)


class Test_sum_evens(unittest.TestCase):
    def test_1(self):
        numbers = []
        actual = sum_evens(numbers)
        expected = 0
        self.assertEqual(actual, expected)

    def test_2(self):
        numbers = [1,2,3,4]
        actual = sum_evens(numbers)
        expected = 6
        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
