import unittest

def only_the_red(cars):
    results = []

    for car in cars:
        if car['color'] == 'red':
            results.append(car)

    return results


def old_guzzlers(cars):
    results = []

    for car in cars:
        if car['year'] < 1980 and car['mpg'] < 12:
            results.append(car)

    return results


def big_ones(cars):
    results = []

    for car in cars:
        if car['length'] > 25 or car['max_weight'] > 4000:
            results.append(car)

    return results


def sort_by_make(cars):
    results = {}

    for car in cars:
        make = car['make']
        if make not in results:
            results[make] = []
        results[make].append(car)

    return results


class Test_only_the_red(unittest.TestCase):
    def test_1_red(self):
        cars = [
            {"type": "truck", "color": "rusted", "year": 1955},
            {"type": "sedan", "color": "red", "year": 2015},
            {"type": "wagon", "color": "pea green", "year": 1965},
            {"type": "truck", "color": "red", "year": 1995},
        ]
        actual = only_the_red(cars)
        expected = [
            {"type": "sedan", "color": "red", "year": 2015},
            {"type": "truck", "color": "red", "year": 1995},
        ]
        self.assertEqual(actual, expected)


class Test_old_guzzlers(unittest.TestCase):
    def test_2_guzzlers(self):
        cars = [
            {"make": "ford", "mpg": 8, "year": 1955},
            {"make": "ford", "mpg": 12, "year": 1964},
            {"make": "ford", "mpg": 11, "year": 1978},
            {"make": "ford", "mpg": 19, "year": 2000},
        ]
        actual = old_guzzlers(cars)
        expected = [
            {"make": "ford", "mpg": 8, "year": 1955},
            {"make": "ford", "mpg": 11, "year": 1978},
        ]
        self.assertEqual(actual, expected)


class Test_big_ones(unittest.TestCase):
    def test_3_bigones(self):
        cars = [
            {"make": "ford", "length": 15, "max_weight": 2000},
            {"make": "ford", "length": 12, "max_weight": 5000},
            {"make": "ford", "length": 30, "max_weight": 2700},
            {"make": "ford", "length": 19, "max_weight": 6000},
        ]
        actual = big_ones(cars)
        expected = [
            {"make": "ford", "length": 12, "max_weight": 5000},
            {"make": "ford", "length": 30, "max_weight": 2700},
            {"make": "ford", "length": 19, "max_weight": 6000},
        ]
        self.assertEqual(actual, expected)


class Test_sort_by_make(unittest.TestCase):
    def test_4_sorted(self):
        cars = [
            {"make": "ford", "model": 8, "year": 1955},
            {"make": "vw", "model": 8, "year": 1963},
            {"make": "chevy", "model": 12, "year": 1964},
            {"make": "ford", "model": 11, "year": 1978},
            {"make": "vw", "model": 8, "year": 2020},
            {"make": "yugo", "model": 8, "year": 1970},
            {"make": "chevy", "model": 19, "year": 2000},
        ]
        actual = sort_by_make(cars)
        expected = {
            "ford": [
                {"make": "ford", "model": 8, "year": 1955},
                {"make": "ford", "model": 11, "year": 1978},
            ],
            "chevy": [
                {"make": "chevy", "model": 12, "year": 1964},
                {"make": "chevy", "model": 19, "year": 2000},
            ],
            "vw": [
                {"make": "vw", "model": 8, "year": 1963},
                {"make": "vw", "model": 8, "year": 2020},
            ],
            "yugo": [
                {"make": "yugo", "model": 8, "year": 1970},
            ]
        }
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
