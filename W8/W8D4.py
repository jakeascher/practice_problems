import unittest

def sub_total(bill):
    """Calculate the subtotal of a bill of line items."""
    sub_total = 0
    for line_item in bill:
        sub_total += line_item['quantity'] * line_item['item_cost']
    return sub_total

    # USING GENERATOR EXPRESSION (https://peps.python.org/pep-0289/)
    # return sum(line_item['item_cost'] * line_item['quantity'] for line_item in bill)


def taxable_items(bill):
    """Return a list of the taxable line items in a bill."""
    items = []
    for item in bill:
        if item['taxable']:
            items.append(item)
    return items

    # USING LIST COMPREHENSION
    # return [line_item for line_item in bill if line_item['taxable']]

    # USING FILTER (https://realpython.com/python-filter-function/)
    # def is_taxable(line_item):
    #     return line_item['taxable']
    # return list(filter(is_taxable, bill))

def taxable_sub_total(bill):
    """Return the taxable subtotal of a bill."""
    taxable_subtotal = 0
    for line_item in bill:
        if line_item['taxable']:
            taxable_subtotal += line_item['quantity'] * line_item['item_cost']
    return taxable_subtotal

    # CLEVER APPROACH USING PREVIOUS FUNCTIONS
    # return sub_total(taxable_items(bill))

def taxes(bill, tax_rate):
    """Calculate a bill's taxes using the provided tax rate."""
    sub_total = 0
    for line_item in bill:
        if line_item['taxable']:
            sub_total += line_item['quantity'] * line_item['item_cost']
    return sub_total * tax_rate

    # CLEVER APPROACH USING PREVIOUS FUNCTIONS
    # return tax_rate * taxable_sub_total(bill)

def bill_total(bill, tax_rate):
    """Calculate a bill's total."""
    sub_total = 0
    taxable_total = 0
    for line_item in bill:
        line_total = line_item['quantity'] * line_item['item_cost']
        sub_total += line_total
        if line_item['taxable']:
            taxable_total += line_total
    return sub_total + (taxable_total * tax_rate)

    # CLEVER APPROACH USING PREVIOUS FUNCTIONS
    # return sub_total(bill) + taxes(bill, tax_rate)


class Test_sub_total(unittest.TestCase):
    def test_1(self):
        bill = [
            {"quantity": 3, "item_cost": 4.00, "taxable": True},
            {"quantity": 7, "item_cost": 1.00, "taxable": True},
            {"quantity": 1, "item_cost": 10.00, "taxable": True},
        ]
        actual = sub_total(bill)
        expected = 29
        self.assertEqual(actual, expected)

class Test_taxable_items(unittest.TestCase):
    def test_2(self):
        bill = [
            {"quantity": 3, "item_cost": 4.00, "taxable": False},
            {"quantity": 7, "item_cost": 1.00, "taxable": False},
            {"quantity": 4, "item_cost": 10.00, "taxable": True},
            {"quantity": 1, "item_cost": 10.00, "taxable": False},
        ]
        actual = taxable_items(bill)
        expected = [{"quantity": 4, "item_cost": 10.00, "taxable": True}]
        self.assertEqual(actual, expected)

class Test_taxable_sub_total(unittest.TestCase):
    def test_3(self):
        bill = [
            {"quantity": 3, "item_cost": 4.00, "taxable": True},
            {"quantity": 7, "item_cost": 1.00, "taxable": False},
            {"quantity": 4, "item_cost": 10.00, "taxable": True},
            {"quantity": 1, "item_cost": 10.00, "taxable": True},
        ]
        actual = taxable_sub_total(bill)
        expected = 62
        self.assertEqual(actual, expected)


class Test_taxes(unittest.TestCase):
    def test_4(self):
        bill = [
            {"quantity": 3, "item_cost": 4.00, "taxable": True},
            {"quantity": 7, "item_cost": 1.00, "taxable": False},
            {"quantity": 3, "item_cost": 10.00, "taxable": True},
            {"quantity": 1, "item_cost": 10.00, "taxable": True},
        ]
        actual = taxes(bill, 0.1)
        expected = 5.2
        self.assertEqual(actual, expected)

class Test_bill_total(unittest.TestCase):
    def test_5(self):
        bill = [
            {"quantity": 3, "item_cost": 4.00, "taxable": True},
            {"quantity": 7, "item_cost": 1.00, "taxable": False},
            {"quantity": 3, "item_cost": 11.00, "taxable": True},
            {"quantity": 1, "item_cost": 10.00, "taxable": True},
        ]
        actual = bill_total(bill, 0.1) # 55/62 62 + 5.5 = 67.5
        expected = 67.5
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
