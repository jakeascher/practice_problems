function assertObjectEquals(actual, expected) {
  actualStringified = JSON.stringify(actual);
  expectedStringified = JSON.stringify(expected);

  if (actualStringified !== expectedStringified) {
    console.log(`❌ Expected ${expectedStringified} but got ${actualStringified}`);
  } else {
    console.log(`✅ Got expected value: ${actualStringified}`);
  }
}

function countWords(str) {
  if (!str) {
    return result;
  }

  const result = {};
  const words = str.split(' ');

  for (let word of words) {
    if (!(word in result)) {
      result[word] = 0;
    }
    result[word] += 1;
  }

  return result;
}

function select(arr, obj) {
  const result = {};

  for (let item of arr) {
    if (item in obj) {
      result[item] = obj[item];
    }
  }
  return result;
}

assertObjectEquals(countWords("hello hello"), { hello: 2 });
assertObjectEquals(countWords("hello hello hello"), { hello: 3 });
assertObjectEquals(countWords("hello world"), { hello: 1, world: 1 });

assertObjectEquals(select(["a", "b", "c"], { a: 1, b: 2, c: 3 }), { a: 1, b: 2, c: 3 });
assertObjectEquals(select(["a", "b", "c"], { a: 1, b: 2, c: 3, d: 4 }), { a: 1, b: 2, c: 3 });
assertObjectEquals(select(["a", "b", "c"], { a: 1, b: 2 }), { a: 1, b: 2 });
