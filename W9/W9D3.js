function assertEquals(actual, expected) {
  if (actual !== expected) {
    console.log(`❌ Expected ${expected} but got ${actual}`);
  } else {
    console.log(`✅ Got expected value: ${actual}`);
  }
}

function repeats(s) {
  let half = s.length / 2;
  let firstHalf = s.slice(0, half);
  let secondHalf = s.slice(half);

  return firstHalf === secondHalf;

  // ALTERNATIVE SOLUTION USING FOR LOOP
  // const length = s.length;
  // const half = Math.floor(length / 2);
  // for (let i = 0; i < half; i += 1) {
  //   if (s[i] != s[half + i]) {
  //     return false;
  //   }
  // }
  // return true;
}

assertEquals(repeats("hellohello"), true);
assertEquals(repeats("helloworld"), false);
assertEquals(repeats("helloworldhello"), false);
assertEquals(repeats("this is a repeat.this is a repeat."), true);
