import unittest

def repeats(s):
    length = len(s)
    first_half = s[0:length // 2]
    second_half = s[length // 2:]

    return first_half == second_half

    # ALTERNATIVE SOLUTION USING FOR LOOP
    # length = len(s)
    # half = length // 2

    # for index, letter in enumerate(s):
    #     if index >= half:
    #         break
    #     if s[index] != s[index + half]:
    #         return False
    # return True


def third_largest_value(values):
    if len(values) < 3:
        return None

    sorted_values = sorted(values)
    return sorted_values[-3]

    # ALTERNATIVE SOLUTION USING SORT LIST METHOD
    # if len(values) < 3:
    #     return None

    # values.sort()
    # return values[-3]


class TestRepeats(unittest.TestCase):
    def test_empty_string(self):
        input = ""
        self.assertTrue(repeats(input))

    def test_repeated_strings(self):
        input = "bahbah"
        self.assertTrue(repeats(input))

        input = "nananananananana"
        self.assertTrue(repeats(input))

    def test_non_repeated_strings(self):
        input = "bahba"
        self.assertFalse(repeats(input))

        input = "nananananann"
        self.assertFalse(repeats(input))


class TestThirdLargestValue(unittest.TestCase):
    def test_empty_list(self):
        input = []
        expected = None
        self.assertEqual(
          third_largest_value(input),
          expected,
        )

    def test_one_item_list(self):
        input = [1]
        expected = None
        self.assertEqual(
          third_largest_value(input),
          expected,
        )

    def test_two_item_list(self):
        input = [1, 2]
        expected = None
        self.assertEqual(
          third_largest_value(input),
          expected,
        )

    def test_three_item_list(self):
        input = [3, 1, 2]
        expected = 1
        self.assertEqual(
          third_largest_value(input),
          expected,
        )

    def test_four_item_list(self):
        input = [17, 3, 1, 2]
        expected = 2
        self.assertEqual(
          third_largest_value(input),
          expected,
        )

    def test_large_list(self):
        input = list(range(1000))
        expected = 997
        self.assertEqual(
          third_largest_value(input),
          expected,
        )

if __name__ == '__main__':
    unittest.main()
