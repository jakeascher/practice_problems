function assertEquals(actual, expected) {
    if (actual !== expected) {
      console.log(`❌ Expected ${expected} but got ${actual}`);
    } else {
      console.log(`✅ Got expected value: ${actual}`);
    }
  }

function median(numbers) {
    const sorted = [...numbers].sort((a, b) => a - b);
    const mid = Math.floor(numbers.length / 2);

    if (sorted.length % 2 === 0) {
        return (sorted[mid - 1] + sorted[mid]) / 2;
    } else {
        return sorted[mid];
    }
}

function mode(numbers) {
    let obj = {};
    for (let i = 0; i < numbers.length; i++) {
        if (obj[numbers[i]]) {
            obj[numbers[i]] += 1;
        } else {
            obj[numbers[i]] = 1;
        }
    }

    let biggestVal = 0;
    let biggestValKey = 0;
    Object.keys(obj).forEach(key => {
        let val = obj[key];
        if (val > biggestVal) {
            biggestVal = val;
            biggestValKey = key;
        }
    });
    return Number(biggestValKey);
}

assertEquals(median([1, 10, 2, 8, 4]), 4);
assertEquals(median([1, 10, 2, 8, 4, 6]), 5);

assertEquals(mode([1, 4, 10, 2, 8, 4, 6, 4]), 4);
