import unittest

def is_anagram(string_1, string_2):
    if string_1 == "" and string_2 == "":
        return False

    string_1 = string_1.lower()
    string_2 = string_2.lower()
    string_1 = string_1.replace(" ", "")
    string_2 = string_2.replace(" ", "")
    string_1 = "".join(char for char in string_1 if char.isalpha())
    string_2 = "".join(char for char in string_2 if char.isalpha())

    if len(string_1) != len(string_2):
        return False

    string_1_dict = {}
    string_2_dict = {}

    for letter in string_1:
        string_1_dict[letter] = string_1_dict.get(letter, 0) + 1

    for letter in string_2:
        string_2_dict[letter] = string_2_dict.get(letter, 0) + 1

    for key in string_1_dict:
        if key not in string_2_dict or string_1_dict[key] != string_2_dict[key]:
            return False

    return True


class Test_is_anagram(unittest.TestCase):
    def test_1(self):
        self.assertEqual(is_anagram("taco", "coat"), True)

    def test_2(self):
        self.assertEqual(is_anagram("TACO", "coat"), True)

    def test_3(self):
        self.assertEqual(is_anagram("t a c o", "coat"), True)

    def test_4(self):
        self.assertEqual(is_anagram("taco!", "coat"), True)

    def test_5(self):
        self.assertEqual(is_anagram("taco", "cat"), False)

    def test_6(self):
        self.assertEqual(is_anagram("cat", "taco"), False)

    def test_7(self):
        self.assertEqual(is_anagram("taco", "tacoxyz"), False)

    def test_8(self):
        self.assertEqual(is_anagram("", ""), False)

if __name__ == '__main__':
    unittest.main()
